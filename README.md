# TicTacToe

## Versión de Angular

Este proyecto ha sido realizado con Angular CLI en la versión 11.2.11.

## Conexión a firebase

La información de la conexión a la base de datos de Firebase se encuentra en el archivo `environments.ts`.

## Descarga de módulos

Para descargar los módulos una vez clonado el proyecto, se debe usar el comando `npm install` en la terminal, así el proyecto podrá ejecutarse.

## Despliegue

Para desplegar el pryecto es necesario ingresar el comando `ng build`, lo que generará un código base listo para despliegue en la carpeta `dist/`.
El contenido de la carpeta `dist/` será el que se debe subir al servidor para desplegar, una vez dentro del servidor es necesario descargar los módulos,
ya que el archivo `.gitignore` no permite cargar los módulos necesarios para el funcionamiento del proyecto.

