import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, Validators } from '@angular/forms';
import { RoomService } from 'src/app/core/services/room/room.service';
import { Observable } from 'rxjs';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { Router } from '@angular/router';



@Component({
  selector: 'app-name-field',
  templateUrl: './name-field.component.html',
  styleUrls: ['./name-field.component.sass']
})
export class NameFieldComponent implements OnInit {

  public playerForm = this.fb.group({
    name: [null, Validators.required],
    code: [null]
  });
  spinner = false;
  rooms: Observable<any[]>;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    public dialogRef: MatDialogRef<NameFieldComponent>,
    private roomService: RoomService,
    private fb: FormBuilder,
    private router: Router,
    firestore: AngularFirestore) {
      this.rooms = firestore.collection('rooms').valueChanges();
     }

  ngOnInit(): void {
    if (this.data.host === false) { // Reconoce si un jugador es anfitrion o invitado
      this.playerForm.get('name').setValue('Jugador 2');
      this.playerForm.get('code').setValidators([Validators.required]);
    } else {
      this.playerForm.get('name').setValue('Jugador 1');
    }
  }

  close(): void {
    this.dialogRef.close();
  }

  submit(): void{
    if (this.playerForm.valid) {
      this.spinner = true;
      if (this.data.host) { // En caso de ser anfitrion, se crea una nueva sala
        this.roomService.addRoom({player_one: this.playerForm.get('name').value}).then(res => {
          const id = (res as DocumentReference).id;
          this.spinner = false;
          this.dialogRef.close();
          this.router.navigate(['room', id] );
        }).catch(error => {
          this.spinner = false;
        });
      }
      else { // En caso de ser invitado, se solicita el código y se actualiza el documento
        this.roomService.updateRoom({player_two: this.playerForm.get('name').value}, this.playerForm.get('code').value).then(res => {
          this.spinner = false;
          this.dialogRef.close();
          this.router.navigate(['room', this.playerForm.get('code').value] );
        }).catch(error => {
          this.spinner = false;
        });
      }
    }
    else {
      this.playerForm.markAllAsTouched();
    }
  }
}
