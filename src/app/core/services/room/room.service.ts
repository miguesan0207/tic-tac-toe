import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class RoomService {

  constructor(private firestore: AngularFirestore) {}

  addRoom(room: any): Promise<any> {
    return this.firestore.collection('rooms').add(room);
  }

  getRoom(id: string) {
    return this.firestore.collection('rooms').doc(id).valueChanges();
  }

  updateRoom(data, id) {
    return this.firestore.collection('rooms').doc(id).update(data);
  }

  deleteRoom(id) {
    return this.firestore.collection('rooms').doc(id).delete();
  }
}
