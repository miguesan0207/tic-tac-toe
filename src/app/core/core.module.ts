import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeskModule } from '../desk/desk.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DeskModule
  ]
})
export class CoreModule { }
