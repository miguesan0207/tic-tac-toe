import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RoomService } from 'src/app/core/services/room/room.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.sass']
})
export class RoomComponent implements OnInit {

  public id: string;
  public room;
  constructor(public route: ActivatedRoute,
              private roomService: RoomService,
              private router: Router) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id'); // El id se obtiene de la url
    this.roomService.getRoom(this.id).subscribe(res => {
      this.room = res;
    });
  }

  exitRoom() { // Al salir de la sala, esta se elimina
    this.roomService.deleteRoom(this.id);
    this.router.navigate(['/home']);
  }

}
