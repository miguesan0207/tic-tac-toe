import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-win',
  templateUrl: './win.component.html',
  styleUrls: ['./win.component.sass']
})
export class WinComponent implements OnInit {

  spinner = false;
  constructor(@Inject(MAT_DIALOG_DATA) public data,
              public dialogRef: MatDialogRef<WinComponent>
              ) { }

  ngOnInit(): void {
  }

  close(): void {
    this.dialogRef.close();
  }

  playAgain() { // Envia información a la sala de que el jugador desea jugar de nuevo
    this.dialogRef.close({newGame: true});
  }

}
