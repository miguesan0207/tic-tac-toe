import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DeskComponent } from './desk.component';
import { RoomComponent } from './room/room.component';
// import { DeskComponent } from './desk.component';

const routes: Routes = [
  { path: 'home', component: DeskComponent},
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'room/:id', component: RoomComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeskRoutingModule { }
