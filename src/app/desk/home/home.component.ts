import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NameFieldComponent } from 'src/app/shared/name-field/name-field.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  constructor(
    public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  
  openNameDialog(host): void { // Abrir el componente para ingresar nombre y crear sala
    this.dialog.open( NameFieldComponent, {
      data: {
        host
      },
      disableClose: true
    });
  }
}
