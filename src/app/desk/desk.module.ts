import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DeskRoutingModule } from './desk-routing.module';
import { MaterialModule } from '../core/material/material.module';
import { HomeComponent } from './home/home.component';
import { DeskComponent } from './desk.component';
import { SharedModule } from '../shared/shared.module';
import { RoomComponent } from './room/room.component';
import { BoardComponent } from './board/board.component';
import { WinComponent } from './win/win.component';


@NgModule({
  declarations: [
    HomeComponent,
    DeskComponent,
    RoomComponent,
    BoardComponent,
    WinComponent
  ],
  imports: [
    MaterialModule,
    CommonModule,
    DeskRoutingModule,
    SharedModule
  ],
  exports: [
    // NavComponent
  ]
})
export class DeskModule { }
