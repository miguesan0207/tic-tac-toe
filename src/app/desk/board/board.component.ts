import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { WinComponent } from '../win/win.component';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.sass']
})
export class BoardComponent implements OnInit {

  @Input() player_one;
  @Input() player_two;
  public game = 0; // Contador para saber que jugador debe empezar
  public turn; // El jugador que lleva el turno
  public winner = null;
  public draw = false;
  public tiles = new Array(9).fill(null); // Espacios de cada celda nulos
  public conditionWins = [ // Array con todas las posibles configuraciones para ganar
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
    ];

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getPlayerTurn();
  }

  getPlayerTurn() { // Este metodo permite definir quién llevará el turno al comienzo del juego
    if (this.game % 2 === 0) {
      this.turn = this.player_one;
    }
    else {
      this.turn = this.player_two;
    }
  }

  clicked(i: number): void { // Permite asignar un espacio segun la casilla seleccionada con el nombre del jugador
    this.tiles[i] = this.turn;
    if (this.turn === this.player_one) { // Cambio de turno
      this.turn = this.player_two;
    }else {
      this.turn = this.player_one;
    }
    this.checkWinner(); // Revisa si existe un ganador

    const draw = this.tiles.find(element => element === null); // Revisa si hay empate
    if (draw === undefined && this.winner === null) {
      this.draw = true;
    }
  }

  playAgain() {
    this.game++;
    this.draw = false;
    this.winner = null;
    this.tiles = new Array(9).fill(null);
    this.getPlayerTurn();
  }

  checkWinner() { // Almacena el valor de la casilla en 3 variables, luego las compara y si tienen el mismo valor, exisate un ganador

    this.conditionWins.forEach((element, i) => {
      const condition = this.conditionWins[i];
      const a = this.tiles[condition[0]]; // Las casillas llevan los nombres de los jugadores
      const b = this.tiles[condition[1]];
      const c = this.tiles[condition[2]];
      if (a !== null || b !== null || c !== null) {
        if (a === b && b === c) {
          this.winner = a;
          this.dialog.open( WinComponent, {
            data: {
              winner: this.winner
            },
            disableClose: true
          }).afterClosed().subscribe(res => { // En el caso de aceptar un nuevo juego, este restablece las casillas y cambia de turno inicial
            if (res) {
              if (res.newGame) {
              this.game++;
              this.winner = null;
              this.draw = false;
              this.tiles = new Array(9).fill(null);
              this.getPlayerTurn();
            }
            }
          });
        }
      }
    });
  }

}
